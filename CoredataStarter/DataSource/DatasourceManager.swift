//
//  DatasourceManager.swift
//  Ashcourt
//
//  Created by Subarna Santra on 19/05/2016.
//  Copyright © 2016 Big Orange Software. All rights reserved.
//


import Foundation
import CoreData

/*protocol Model {
    func setModelId(modelId: Int)
    func mapData(json: [String: AnyObject], updateTime: Double)
}

protocol Query {
    
}

protocol ReadableDatabase {
    func loadObjects<T: Model>(matching query: Query) -> [T]?
    func loadObject<T: Model>(withID id: String) -> T?
}

protocol WritableDatabase {
    func saveObject<T: Model>(_ object: T)
    func saveObjects<T: Model>(_ object: T)
    func saveContext (callback: @escaping (_ saved: Bool, _ message: String) -> ())
}

protocol Dejsonable {
    func mapData(json: Dejsonize)
}

protocol UpdateTimeProtocol:class {
    var updateTime: NSNumber? {get set}
    func setUpdateTime(updateTime: Double)
}

extension UpdateTimeProtocol {
    
    func setUpdateTime(updateTime: Double) {
        if (updateTime > 0) {
            self.updateTime = NSNumber(value: updateTime)
        }
    }
}

protocol UpsertModelProtocol {
    init()
    init?(in context: NSManagedObjectContext, from json: [String: AnyObject], updateTime: Double)
    init?(in context: NSManagedObjectContext, from json: [String: AnyObject], updateTime: Double, insertOfUpdate:Bool)
//    func upsert(in context: NSManagedObjectContext, from json: [String: AnyObject], updateTime: Double)
//    func insert(in context: NSManagedObjectContext, from json: [String: AnyObject], updateTime: Double)
//    func update(in context: NSManagedObjectContext, from json: [String: AnyObject], updateTime: Double)
}
protocol DejsonableModelProtocol:Model, Dejsonable, UpdateTimeProtocol {
    
    
    
}


extension UpsertModelProtocol {
    
//    init() {
//        
//    }
//    init?(in context: NSManagedObjectContext, from json: [String: AnyObject], updateTime: Double, insertOfUpdate:Bool) {
//        self.init()
//        if (insertOfUpdate) {
//           // self.upsert(json: json, updateTime: updateTime)
//        } else {
//           // self.insert(json: json, updateTime: updateTime)
//        }
//        return nil
//    }
}

extension DejsonableModelProtocol {
    
    func mapData(json: [String: AnyObject], updateTime: Double) {
        if let dejsonisedData = Dejsonize(data: json) {
            self.mapData(json: dejsonisedData)
            self.setUpdateTime(updateTime: updateTime)
        }
    }
}

typealias Database = ReadableDatabase & WritableDatabase

extension NSManagedObjectContext: Database {
    
    func loadObjects<T: Model>(matching query: Query) -> [T]? {
        return nil
        
    }
    func loadObject<T: Model>(withID id: String) -> T? {
        return nil
    }
    
    func saveObject<T: Model>(_ object: T) {
        
    }
    
    func saveObjects<T: Model>(_ object: T) {
        
    }
    
    func saveList(in modelHelper: ModelHelper, from fetchedRecordList : Array<Dictionary<String,AnyObject>>, offset:Int = 0, updateData : Bool = false, resolveData: @escaping (_ found: Bool, _ list: AnyObject?, _ message: String) -> ()){
        
        let updateTime : Double = (updateData == true) ? Date().timeIntervalSince1970 : 0
        
        for record in fetchedRecordList {
            _ = modelHelper.upsert(in: self, from: record, updateTime: updateTime)
        }
        
        if (updateData == true) {
            modelHelper.clearOutdatedData(in: self, updateTime: updateTime)
        }
        
        self.saveContext(callback: { (saved, message) in
            resolveData(true, nil, message)
        })
    }
    
    func saveContext (callback: @escaping (_ saved: Bool, _ message: String) -> ()) {
        
        DatasourceManager.sharedInstance.saveCurrentContext(self, callback: { (saved, message) in
            callback(saved, message)
        })
    }
    
}*/


class AppCoredata1 {
    
    fileprivate var coredataStarter: CoredataStarter?
    
    convenience init(coreDataFileName:String, resourceName:String) {
        
        let libraryDirectoryUrl = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
        let storeUrl            = libraryDirectoryUrl.appendingPathComponent(coreDataFileName)
        
        guard let modelUrl      = Bundle.main.url(forResource: resourceName, withExtension: "momd") else {
            fatalError("Error loading Core Data model")
        }
        
        self.init(modelUrl: modelUrl, storeUrl: storeUrl)
    }
    
    init(modelUrl:URL, storeUrl:URL) {
        coredataStarter = CoredataStarter(modelUrl: modelUrl, storeUrl: storeUrl, storeCoordinationSystem: StoreCoordinatorSystem.OneCoordinatorSystem)
        self.addObserverForMergeNotification()
    }
    
    func addObserverForMergeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.mergeChangesFromSaveNotification(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
    }
    
    
    @objc func mergeChangesFromSaveNotification (_ notification : Notification) {
        
        self.inMainContext { (context) in
            context?.mergeChanges(fromContextDidSave: notification)
        }
        
        self.inPrivateContext { (context) in
            context?.reset()
        }
    }
    
    func inSyncContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        if let sourceControler = coredataStarter {
            sourceControler.inSyncContext(callback: { (context) in
                callback(context)
            })
        } else {
            callback(nil)
        }
    }
    
    
    func inPrivateContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        if let sourceControler = coredataStarter {
            sourceControler.inPrivateContext(callback: { (context) in
                callback(context)
            })
        } else {
            callback(nil)
        }
    }
    
    func inMainContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        if let sourceControler = coredataStarter {
            sourceControler.inMainContext(callback: { (context) in
                callback(context)
            })
        } else {
            callback(nil)
        }
    }
    
    
    func saveContext(callback: @escaping (_ saved: Bool, _ message: String) -> ()) {
        if let sourceControler = coredataStarter {
            sourceControler.saveWriterContext(callback: { (saved, message) in
                callback(saved, message)
            })
        } else {
            callback(false, "could not finid database")
        }
    }
    
    
    func resetDatabase(_ callback: @escaping (_ done: Bool, _ message: String) -> ()) {
        
        if let sourceControler = coredataStarter {
            sourceControler.resetPersistentStoreCoordinator(callback: callback)
        } else {
            callback(false, "could not finid database")
        }
    }

}

class DatasourceManager {
    
    
    fileprivate var dataStoreController: DataStoreController?
    
    
    var fetchedResultController: NSFetchedResultsController<NSFetchRequestResult>?
    

    
    
    static let sharedInstance = DatasourceManager()
    
    
    fileprivate init() {
        
        let libraryDirectoryUrl = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
        
        let storeUrl = libraryDirectoryUrl.appendingPathComponent("SingleViewCoreData.sqlite")
        
        guard let modelUrl = Bundle.main.url(forResource: "Ashcourt", withExtension: "momd") else {
            fatalError("Error loading Core Data model")
        }
        
        dataStoreController = DataStoreController(modelUrl: modelUrl, storeUrl: storeUrl)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(DatasourceManager.mergeChangesFromSaveNotification(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
        
      
        
    }
    
    
    func resetDatabase(_ callback: @escaping (_ done: Bool, _ message: String) -> ()) {
        
        dataStoreController!.resetPersistentStoreCoordinator(callback: callback)
        
    }
    
    
    @objc func mergeChangesFromSaveNotification (_ notification : Notification) {
        
        self.inMainContext { (context) in
            
            context?.mergeChanges(fromContextDidSave: notification)
        }
        
        self.inPrivateContext { (context) in

            context?.reset()
        }
        
    }
    
    func saveCurrentContext (_ currentContext : NSManagedObjectContext, callback: @escaping (_ saved: Bool, _ message: String) -> ()) {
        
        dataStoreController?.saveCurrentContext(currentContext: currentContext, callback: callback)
    }
    
    
    func getLocalEntity(_ currentContext : NSManagedObjectContext, entityName: String, useTempContext : Bool = false)-> NSEntityDescription? {
        
            let entityLocal: NSEntityDescription = NSEntityDescription.entity(forEntityName: entityName, in: currentContext)!
            
            return entityLocal
    }
    
    
    
    
    func clearEntity(_ currentContext : NSManagedObjectContext, entityName: String, predicate: NSPredicate? = nil, saveData : Bool = true){
        
                    
        if let fetchedRecordList: NSArray = self.standardQuery(currentContext, entityName : entityName, predicate: predicate, sortDescriptor: nil, includesPropertyValues : false) as NSArray? {
            
            for recordObj in fetchedRecordList {
                
                currentContext.delete(recordObj as! NSManagedObject)
            }
            
            if (saveData == true && currentContext == dataStoreController?.managedObjectContext) {
                
                self.saveCurrentContext(currentContext, callback: { (saved, message) in
                    
                })
                
            }
            
        }
    
    }
    
    
    func countGroupedEntity(_ currentContext : NSManagedObjectContext, entityName: String, predicate: NSPredicate? = nil, groupBy: String? = nil) -> Int?{
        
        if let request = getFetchRequest(currentContext, entityName: entityName, predicate:  predicate, sortDescriptor: nil, offset: 0, fetchLimit : 0, includesPropertyValues : true) {
            
            
            if (groupBy != nil) {
                
                request.resultType = .dictionaryResultType
                
                request.propertiesToGroupBy = [groupBy!]
                
                request.returnsDistinctResults = true
                
                request.propertiesToFetch = [groupBy!]
                
                do {
                    
                    
                    var totalCount = 0
                    
                    let result  = try currentContext.fetch(request)
                    
                    if let resultDict = result as? Array<Dictionary<String, AnyObject>> {
                        
                        totalCount = resultDict.count
                    }
                    
                    return totalCount
                    // success ...
                }
                catch let error as NSError {
                    // failure
                    print("Fetch failed: \(error.localizedDescription)")
                    
                    return nil
                    
                }
                
            } else {
                
                if let count  = try? currentContext.count(for: request) {
                    return count
                }
            }
        }
        
        return nil
    }
    
    
    func countEntity(_ currentContext : NSManagedObjectContext, entityName: String, predicate: NSPredicate? = nil) -> Int?{
        
        if let request = getFetchRequest(currentContext, entityName: entityName, predicate:  predicate, sortDescriptor: nil, offset: 0, fetchLimit : 0, includesPropertyValues : false) {
            
            
            if let count  = try? currentContext.count(for: request) {
                return count
            }
        }
        
        return nil
    }
    
    
    func standardSearchQuery(_ currentContext : NSManagedObjectContext, entityName: String, predicate: NSPredicate? = nil, sortDescriptor: NSSortDescriptor? = nil, offset :Int = 0, fetchLimit : Int = 0) -> NSManagedObject?{
        
            
            if let request = getFetchRequest(currentContext, entityName: entityName, predicate:  predicate, sortDescriptor: sortDescriptor, offset : offset, fetchLimit : fetchLimit) {
                
                do {
                    
                    let result  = try currentContext.fetch(request)
                    
                    
                    if ( result.count>0){
                        
                        return result.first as? NSManagedObject
                    }
                    else{
                        return nil
                    }
                    // success ...
                }
                catch let error as NSError {
                    // failure
                    print("Fetch failed: \(error.localizedDescription)")
                    
                }
        }
       
        
        return nil
    }
    
    
    func getFetchRequest(_ currentContext : NSManagedObjectContext, entityName: String, predicate: NSPredicate? = nil, sortDescriptor: NSSortDescriptor? = nil, sortDescriptorList: [NSSortDescriptor]? = nil, offset :Int = 0, fetchLimit : Int = 0, includesPropertyValues : Bool = true) -> NSFetchRequest<NSFetchRequestResult>?{
        
        
        //let entity: NSEntityDescription = NSEntityDescription.entity(forEntityName: entityName, in: currentContext)!
        // let request: NSFetchRequest = NSFetchRequest()
        // request.entity = entity
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        

        
        request.fetchOffset = offset
        
        request.includesPropertyValues = includesPropertyValues

        if (fetchLimit > 0) {

            request.fetchLimit = fetchLimit
        }
        //request.fetchLimit = 1
        
        if (sortDescriptor != nil){
            
            request.sortDescriptors = [sortDescriptor!]
            
        } else if (sortDescriptorList != nil) {
            
            request.sortDescriptors = sortDescriptorList
        }
        
        if(predicate != nil ){
            request.predicate = predicate
        }
        
        return request

    }
    
    
    func standardQuery(_ currentContext : NSManagedObjectContext, entityName: String, predicate: NSPredicate? = nil, sortDescriptor: NSSortDescriptor? = nil, sortDescriptorList: [NSSortDescriptor]? = nil, offset :Int = 0, fetchLimit : Int = 0, includesPropertyValues : Bool = true) -> [AnyObject]?{
        
        
        if let request = getFetchRequest(currentContext, entityName: entityName, predicate:  predicate, sortDescriptor: sortDescriptor, sortDescriptorList : sortDescriptorList, offset : offset, fetchLimit : fetchLimit, includesPropertyValues : includesPropertyValues) {
            
            
            do {
                
                let result: NSArray?  = try currentContext.fetch(request) as NSArray
                
                if let receivedResult = result, receivedResult.count > 0 {
                //if ((result) != nil) && result?.count > 0{
                    return receivedResult as [AnyObject]
                }
                else{
                    return nil
                }
                // success ...
            } catch let error as NSError {
                // failure
                print("Fetch failed: \(error.localizedDescription)")
                
            }
            
        }
            
        
        
        return nil
    }
    
    
    func inSyncContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        
        if (dataStoreController != nil) {
            
            dataStoreController?.inSyncContext(callback: { (context) in
                
                if (context != nil) {
                    
                  callback(context)
                    
                    
                } else {
                    
                    callback(nil)
                }
            
            })
            
        }
    }
    
    
    func inPrivateContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        
        if (dataStoreController != nil) {
            
            dataStoreController?.inPrivateContext(callback: { (context) in
                
                callback(context)
                
            })
            
        }
    }
    
    func inMainContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        
        if (dataStoreController != nil) {
            
            dataStoreController?.inMainContext(callback: { (context) in
                
                callback(context)
                
            })
            
        }
    }
    
    
    func saveContext() {
        
        if (dataStoreController != nil) {
            
            dataStoreController?.saveWriterContext(callback: { (saved, message) in
                
                
            })
            
        }
    }
    
}
