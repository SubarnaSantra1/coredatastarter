//
//  AppCoredata.swift
//
//  Created by Subarna Santra on 20/01/2019.
//  Copyright © 2019 All rights reserved.
//


import Foundation
import CoreData


class CoredataStarter {
    
    private lazy var currentStoreCoordinatorSystem: StoreCoordinatorSystem = StoreCoordinatorSystem.OneCoordinatorSystem;
    private var _storeUrl: URL
    
    private var _managedObjectContext: NSManagedObjectContext
    
    
    var managedObjectContext: NSManagedObjectContext? {
        guard let coordinator = _managedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }
        return _managedObjectContext
    }
    
    
    private var _writerManagedObjectContext: NSManagedObjectContext
    
    var writerManagedObjectContext: NSManagedObjectContext? {
        guard let coordinator = _writerManagedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }
        return _writerManagedObjectContext
    }
    
    
    private var _privateManagedObjectContext: NSManagedObjectContext
    
    var privateManagedObjectContext: NSManagedObjectContext? {
        
        guard let coordinator = _privateManagedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }
        return _privateManagedObjectContext
    }
   
    
    let managedObjectModel: NSManagedObjectModel
    let persistentStoreCoordinator: NSPersistentStoreCoordinator
    let syncPersistentStoreCoordinator: NSPersistentStoreCoordinator
    
    var error: NSError?
    
    func inMainContext(callback: @escaping (NSManagedObjectContext?) -> Void) {
        // Dispatch the request to our serial queue first and then back to the context queue.
        // Since we set up the stack on this queue it will have succeeded or failed before
        // this block is executed.
        
       
        queue.async() {
            guard let context = self.managedObjectContext else {
                callback(nil)
                return
            }
            
            context.perform {
                callback(context)
            }
        }
    }
    
    
    func inPrivateContext(callback: @escaping (NSManagedObjectContext?) -> Void) {
        // Dispatch the request to our serial queue first and then back to the context queue.
        // Since we set up the stack on this queue it will have succeeded or failed before
        // this block is executed.
        queue.async() {
            guard let context = self.privateManagedObjectContext else {
                callback(nil)
                return
            }
            
            context.perform {
                callback(context)
            }
        }
    }
    
    func getSyncContext() -> NSManagedObjectContext? {
        
        let syncManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        syncManagedObjectContext.parent = _writerManagedObjectContext
        
        syncManagedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        return syncManagedObjectContext;
        
    }
    
    func inSyncContext(callback: @escaping (NSManagedObjectContext?) -> Void) {
        // Dispatch the request to our serial queue first and then back to the context queue.
        // Since we set up the stack on this queue it will have succeeded or failed before
        // this block is executed.
        queue.async() {
            guard let context = self.getSyncContext() else {
                DispatchQueue.main.async {
                    callback(nil)
                }
                return
            }
            
            context.perform {
                callback(context)
            }
        }
    }
    
    func saveWriterContext(callback: @escaping (_ saved: Bool, _ error: Error) -> ()) {
        
        queue.async() {
            
            guard let context = self.writerManagedObjectContext else {
                callback(false, AppCoredataError.CoredatSaveError)
                return
            }
            
            self.saveCurrentContext(currentContext: context, callback: callback)
        }
    }
    
    
    func saveCurrentContext (currentContext : NSManagedObjectContext, callback: @escaping (_ saved: Bool, _ error: Error) -> ()) {
        
       // dispatch_async(queue) {
            
        currentContext.perform {
            
            if currentContext.hasChanges {
                
                do {
                    try currentContext.save()
                    
                    if let parentContext = currentContext.parent {
                        
                        self.saveCurrentContext(currentContext: parentContext, callback: callback)
                        
                    } else {
                        DispatchQueue.main.async() {
                            // Update the UI on the main thread.
                            callback(true, AppCoredataError.CoredatNoError)
                            
                        }
                    }
                    
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    
                    DispatchQueue.main.async() {
                        
                        callback(false, error)
                        
                    }
                }
            }
            else {
                DispatchQueue.main.async() {
                    
                    callback(false, AppCoredataError.CoredatSaveError)
                }
            }
        }
        //}
    }
    
    
    func resetPersistentStoreCoordinator(callback: @escaping (_ done: Bool, _ error: Error) -> ()) {
        
        queue.async() {

            if let persistentStore = self.persistentStoreCoordinator.persistentStores.last,
                let syncPersistentStore = self.syncPersistentStoreCoordinator.persistentStores.last { 

                do {
                    
                    let storeUrl = persistentStore.url
                    
                    try self.persistentStoreCoordinator.remove(persistentStore)
                    
                    try self.syncPersistentStoreCoordinator.remove(syncPersistentStore)
                    
                    do {
                        try FileManager.default.removeItem(at: storeUrl!)
                        
                        
                    } catch {
                        callback(false, error)
                        
                    }


                    self.addPersistentStoreToCoordinator(storeUrl: storeUrl!)
                    
                    
                    DispatchQueue.main.async() {
                        
                        callback(true, AppCoredataError.CoredatNoError)
                    }

                    
                    
                } catch {
                    DispatchQueue.main.async() {
                        
                        callback(false, error)
                    }
                }
                
            } else {
                
                DispatchQueue.main.async() {
                    
                    callback(false, AppCoredataError.CoredatResetError)
                }
            }
            
           
        }
        
    }
    
    
    private let queue: DispatchQueue
    
    init(modelUrl: URL, storeUrl: URL, storeCoordinationSystem:StoreCoordinatorSystem) throws {
        
        guard let modelAtUrl = NSManagedObjectModel(contentsOf: modelUrl) else {
            //fatalError("Error initializing managed object model from URL: \(modelUrl)")
            if let error = error {
                throw error
            } else {
                throw AppCoredataError.CoredatModelLoadError
            }
            
        }
        managedObjectModel              = modelAtUrl
        persistentStoreCoordinator      = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        syncPersistentStoreCoordinator  = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        _writerManagedObjectContext     = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        _managedObjectContext           = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        
        if (storeCoordinationSystem == StoreCoordinatorSystem.OneCoordinatorSystem) {
            _writerManagedObjectContext.persistentStoreCoordinator  = persistentStoreCoordinator
            _managedObjectContext.parent                            = _writerManagedObjectContext
        } else {
            _managedObjectContext.persistentStoreCoordinator        = persistentStoreCoordinator
            _writerManagedObjectContext.persistentStoreCoordinator  = syncPersistentStoreCoordinator
        }
        
        _managedObjectContext.mergePolicy           = NSMergeByPropertyObjectTrumpMergePolicy
        _privateManagedObjectContext                = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        _privateManagedObjectContext.parent         = _writerManagedObjectContext
        _privateManagedObjectContext.mergePolicy    = NSMergeByPropertyObjectTrumpMergePolicy
        
        _storeUrl = storeUrl
        queue = DispatchQueue(label: "CoreDataStarterSerialQueue")
        self.addPersistentStoreToCoordinator(storeUrl: storeUrl)
        
        currentStoreCoordinatorSystem = storeCoordinationSystem;
    }
    
    
    func addPersistentStoreToCoordinator(storeUrl : URL) {
        
        
        let options : Dictionary<String, Any> = [
            NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true,
            NSSQLitePragmasOption:["journal_mode" : "DELETE"]
        ]
        
        queue.async() {
            do {
                try self.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: options)
                
                try self.syncPersistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: options)
                
            } catch let error as NSError {
                print("Unable to initialize persistent store coordinator:", error)
                self.error = error
            } catch {
                fatalError()
            }
        }
    }
}
