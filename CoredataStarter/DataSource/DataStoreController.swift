//
//  DataStoreController.swift
//  Ashcourt
//
//  Created by Subarna Santra on 19/05/2016.
//  Copyright © 2016 Big Orange Software. All rights reserved.
//


import Foundation

import CoreData


class DataStoreController {
    
    
    private var syncUserCounter : Int = 0
    
    private var _storeUrl: URL
    
    private var _managedObjectContext: NSManagedObjectContext
    
    
    var managedObjectContext: NSManagedObjectContext? {
        guard let coordinator = _managedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }
        return _managedObjectContext
    }
    
    
    private var _writerManagedObjectContext: NSManagedObjectContext
    
    var writerManagedObjectContext: NSManagedObjectContext? {
        guard let coordinator = _writerManagedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }
        return _writerManagedObjectContext
    }
    
    
    private var _privateManagedObjectContext: NSManagedObjectContext
    
    var privateManagedObjectContext: NSManagedObjectContext? {
        
        guard let coordinator = _privateManagedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }
        return _privateManagedObjectContext
    }
    
    
    private var _syncManagedObjectContext: NSManagedObjectContext
    
    var syncManagedObjectContext: NSManagedObjectContext? {
        
        guard let coordinator = _syncManagedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }
        return _syncManagedObjectContext
    }
    
   
    
    let managedObjectModel: NSManagedObjectModel
    let persistentStoreCoordinator: NSPersistentStoreCoordinator
    let syncPersistentStoreCoordinator: NSPersistentStoreCoordinator
    
    var error: NSError?
    
    func inMainContext(callback: @escaping (NSManagedObjectContext?) -> Void) {
        // Dispatch the request to our serial queue first and then back to the context queue.
        // Since we set up the stack on this queue it will have succeeded or failed before
        // this block is executed.
        
       
        queue.async() {
            guard let context = self.managedObjectContext else {
                callback(nil)
                return
            }
            
            context.perform {
                callback(context)
            }
        }
    }
    
    
    func inPrivateContext(callback: @escaping (NSManagedObjectContext?) -> Void) {
        // Dispatch the request to our serial queue first and then back to the context queue.
        // Since we set up the stack on this queue it will have succeeded or failed before
        // this block is executed.
        queue.async() {
            guard let context = self.privateManagedObjectContext else {
                callback(nil)
                return
            }
            
            context.perform {
                callback(context)
            }
        }
    }
    
    
    /*func getSyncContext() -> NSManagedObjectContext? {
        
        let _syncManagedObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        
        _syncManagedObjectContext.parentContext = _writerManagedObjectContext
        
        _syncManagedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        
        guard let coordinator = _syncManagedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }

        return _syncManagedObjectContext

    }*/
    
    
    func getSyncContext() -> NSManagedObjectContext? {
        
        /*let _syncManagedObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        
        _syncManagedObjectContext.parentContext = _writerManagedObjectContext
        
        _syncManagedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        
        guard let coordinator = _syncManagedObjectContext.persistentStoreCoordinator else {
            return nil
        }
        if coordinator.persistentStores.isEmpty {
            return nil
        }*/
        
        self.syncUserCounter = self.syncUserCounter + 1
        
        return syncManagedObjectContext
        
    }
    
    func inSyncContext(callback: @escaping (NSManagedObjectContext?) -> Void) {
        // Dispatch the request to our serial queue first and then back to the context queue.
        // Since we set up the stack on this queue it will have succeeded or failed before
        // this block is executed.
        queue.async() {
            guard let context = self.getSyncContext() else {
                DispatchQueue.main.async {
                    callback(nil)
                }
                return
            }
            
            context.perform {
                callback(context)
            }
        }
    }
    
    func saveWriterContext(callback: @escaping (_ saved: Bool, _ message: String) -> ()) {
        
        queue.async() {
            
            guard let context = self.writerManagedObjectContext else {
                callback(false, "NO_DATA")
                return
            }
            
            self.saveCurrentContext(currentContext: context, callback: callback)
        }
    }
    
    
    func saveCurrentContext (currentContext : NSManagedObjectContext, callback: @escaping (_ saved: Bool, _ message: String) -> ()) {
        
       // dispatch_async(queue) {
            
        currentContext.perform {
            
            if currentContext.hasChanges {
                
                do {
                    try currentContext.save()
                    
                    if let parentContext = currentContext.parent {
                        
                        if (currentContext == self.syncManagedObjectContext) {
                            
                            if (self.syncUserCounter > 0) {
                                
                                self.syncUserCounter = self.syncUserCounter - 1
                            }
                            
                            if (self.syncUserCounter == 0) {
                                
                                self.syncManagedObjectContext?.reset()
                                
                            }
                        }
                        
                        self.saveCurrentContext(currentContext: parentContext, callback: callback)
                        
                    } else {
                        
                        /*if (currentContext == self.syncManagedObjectContext) {
                            
                            self.syncManagedObjectContext?.reset()
                        }*/
                        
                        
                        DispatchQueue.main.async() {
                            // Update the UI on the main thread.
                            
                            callback(true, "")
                            
                        }
                    }
                    
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    
                    DispatchQueue.main.async() {
                        
                        callback(false, "")
                        
                    }
                }
            }
            else {
                DispatchQueue.main.async() {
                    
                    callback(false, "")
                }
            }
        }
        //}
    }
    
    
    func resetPersistentStoreCoordinator(callback: @escaping (_ done: Bool, _ message: String) -> ()) {
        
        queue.async() {

            if let persistentStore = self.persistentStoreCoordinator.persistentStores.last,
                let syncPersistentStore = self.syncPersistentStoreCoordinator.persistentStores.last { 

                do {
                    
                    let storeUrl = persistentStore.url
                    
                    try self.persistentStoreCoordinator.remove(persistentStore)
                    
                    try self.syncPersistentStoreCoordinator.remove(syncPersistentStore)
                    
                    do {
                        try FileManager.default.removeItem(at: storeUrl!)
                        
                        
                    } catch let error as NSError {

                        print("Unable to remove persistent store file:", error)
                        
                    } catch {
                        //fatalError()
                    }


                    self.addPersistentStoreToCoordinator(storeUrl: storeUrl!)
                    
                    
                    DispatchQueue.main.async() {
                        
                        callback(true, "")
                    }

                    
                    
                } catch let error as NSError {

                    print("Unable to remove persistent store coordinator:", error)
                    
                    DispatchQueue.main.async() {
                        
                        callback(false, "")
                    }
                    
                   
                } catch {
                    //fatalError()
                    
                    DispatchQueue.main.async() {
                        
                        callback(false, "")
                    }
                }
                
            } else {
                
                DispatchQueue.main.async() {
                    
                    callback(false, "")
                }
            }
            
           
        }
        
    }
    
    
    private let queue: DispatchQueue
    
    init(modelUrl: URL, storeUrl: URL, concurrencyType: NSManagedObjectContextConcurrencyType = .mainQueueConcurrencyType) {
        
        guard let modelAtUrl = NSManagedObjectModel(contentsOf: modelUrl) else {
            fatalError("Error initializing managed object model from URL: \(modelUrl)")
        }
        managedObjectModel = modelAtUrl
        
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        syncPersistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        _writerManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        _writerManagedObjectContext.persistentStoreCoordinator = syncPersistentStoreCoordinator
        
        _managedObjectContext = NSManagedObjectContext(concurrencyType: concurrencyType)
        
        //_managedObjectContext.parent = _writerManagedObjectContext
        _managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        _managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        _privateManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        _privateManagedObjectContext.parent = _writerManagedObjectContext
        
        _privateManagedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        
        _syncManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        _syncManagedObjectContext.parent = _writerManagedObjectContext
        
        _syncManagedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        
        
//        store = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error];
//        NSAssert(store, @"Error initializing PSC: %@\n%@", [error localizedDescription], [error userInfo]);

        
        
        
        
      //  print("Initializing persistent store at URL: \(storeUrl.path!)")
        
        _storeUrl = storeUrl
        
//        let options = [
//            NSMigratePersistentStoresAutomaticallyOption: true,
//            NSInferMappingModelAutomaticallyOption: true
//        ]
        
        
        //let dispatch_queue_attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_USER_INITIATED, 0)
        
        
       // queue = dispatch_queue_create("DataStoreControllerSerialQueue", dispatch_queue_attr)
        
        
        queue = DispatchQueue(label: "DataStoreControllerSerialQueue")
        self.addPersistentStoreToCoordinator(storeUrl: storeUrl)
        
        
//        queue.async() {
//            do {
//                try self.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl as URL, options: options)
//            } catch let error as NSError {
//                print("Unable to initialize persistent store coordinator:", error)
//                self.error = error
//            } catch {
//                fatalError()
//            }
//        }
        
        
    }
    
    
    func addPersistentStoreToCoordinator(storeUrl : URL) {
        
        
        let options : Dictionary<String, Any> = [
            NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true,
            NSSQLitePragmasOption:["journal_mode" : "DELETE"]
        ]
        
        queue.async() {
            do {
                try self.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: options)
                
                try self.syncPersistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: options)
                
            } catch let error as NSError {
                print("Unable to initialize persistent store coordinator:", error)
                self.error = error
            } catch {
                fatalError()
            }
        }
    }
}
