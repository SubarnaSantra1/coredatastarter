//
//  AppCoredata.swift
//
//  Created by Subarna Santra on 20/01/2019.
//  Copyright © 2019 All rights reserved.
//

import Foundation
import CoreData

enum StoreCoordinatorSystem {
    case TwoCoordinatorSystem
    case OneCoordinatorSystem
}

enum AppCoredataError : Error {
    case CoredatNoError
    case CoredatSaveError
    case CoredatResetError
    case CoredatNotFoundError
    case CoredatModelLoadError
    case CoredatCreateError
}

class AppCoredata {
    
    fileprivate var coredataStarter: CoredataStarter?
    
    convenience init(resourceName:String, coreDataFileName:String = "SingleViewCoreData.sqlite", storeCoordinationSystem:StoreCoordinatorSystem = StoreCoordinatorSystem.TwoCoordinatorSystem) throws {
        
        let libraryDirectoryUrl = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
        let storeUrl            = libraryDirectoryUrl.appendingPathComponent(coreDataFileName)
        
        guard let modelUrl      = Bundle.main.url(forResource: resourceName, withExtension: "momd") else {
            throw AppCoredataError.CoredatModelLoadError
        }
        
        do {
            try self.init(modelUrl: modelUrl, storeUrl: storeUrl, storeCoordinationSystem:storeCoordinationSystem)
        } catch  {
            throw error
        }
    }
    
    init(modelUrl:URL, storeUrl:URL, storeCoordinationSystem:StoreCoordinatorSystem = StoreCoordinatorSystem.TwoCoordinatorSystem) throws {
        
        do {
            try coredataStarter = CoredataStarter(modelUrl: modelUrl, storeUrl: storeUrl, storeCoordinationSystem: storeCoordinationSystem)
            self.addObserverForMergeNotification()
        } catch  {
            throw error
        }
    }
    
    
    func addObserverForMergeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.mergeChangesFromSaveNotification(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
    }
    
    
    @objc func mergeChangesFromSaveNotification (_ notification : Notification) {
        
        self.inMainContext { (context) in
            context?.mergeChanges(fromContextDidSave: notification)
        }
        
        self.inPrivateContext { (context) in
            context?.reset()
        }
    }
    
    
    open func performBackgroundSyncTask(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        if let sourceControler = coredataStarter {
            sourceControler.inSyncContext(callback: { (context) in
                callback(context)
            })
        } else {
            callback(nil)
        }
    }
    
    
    func inPrivateContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        if let sourceControler = coredataStarter {
            sourceControler.inPrivateContext(callback: { (context) in
                callback(context)
            })
        } else {
            callback(nil)
        }
    }
    
    func inMainContext(_ callback: @escaping (NSManagedObjectContext?) -> Void) {
        if let sourceControler = coredataStarter {
            sourceControler.inMainContext(callback: { (context) in
                callback(context)
            })
        } else {
            callback(nil)
        }
    }
    
    
    func saveContext(callback: @escaping (_ saved: Bool, _ error: Error) -> ()) {
        if let sourceControler = coredataStarter {
            sourceControler.saveWriterContext(callback: { (saved, error) in
                callback(saved, error)
            })
        } else {
            callback(false, AppCoredataError.CoredatNotFoundError)
        }
    }
    
    
    func resetDatabase(_ callback: @escaping (_ done: Bool, _ error: Error) -> ()) {
        
        if let sourceControler = coredataStarter {
            sourceControler.resetPersistentStoreCoordinator(callback: callback)
        } else {
            callback(false, AppCoredataError.CoredatNotFoundError)
        }
    }

}
